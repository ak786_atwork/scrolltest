package com.example.scrolltest

import android.app.Application

class MyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object {
        lateinit var context: MyApp
            private set
    }
}