package com.example.scrolltest

import android.util.DisplayMetrics
import kotlin.math.roundToInt


fun getList(): ArrayList<String> {
    val list = ArrayList<String>()

    for (i in 0..100) {
        list.add("hello $i")
    }

    return list
}

fun dpToPx(dp: Int): Int {
    val displayMetrics: DisplayMetrics = MyApp.context.resources.displayMetrics
    return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}