package com.example.scrolltest

import android.animation.Animator
import android.animation.ValueAnimator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    private val SEARCH_BAR_HEIGHT = dpToPx(70)  //replace it with view height

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        var isSearchVisible = false
        var isAnimating = false
        var lastPosition = -1
        var currentPosition = 0
        var isScrollingUp = false

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            currentPosition =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
            Log.d(TAG, "current position = $currentPosition last Position = $lastPosition")

            if (currentPosition < lastPosition) {
                //scroll up
                if (!isScrollingUp || isSearchVisible || isAnimating) {
                    isScrollingUp = true
                    lastPosition = currentPosition
                    return
                }

                isAnimating = true
                isScrollingUp = true

                animate(searchView, 0, SEARCH_BAR_HEIGHT, object : AnimationListener {
                    override fun onAnimationCompleted() {
                        isAnimating = false
                        isSearchVisible = true
                    }

                    override fun onAnimationCanceled() {
                        isAnimating = false
                    }
                })
            } else if (currentPosition > lastPosition) {
                //scroll down
                if (isScrollingUp || !isSearchVisible || isAnimating) {
                    isScrollingUp = false
                    lastPosition = currentPosition
                    return
                }

                isAnimating = true
                isScrollingUp = false
                animate(searchView, SEARCH_BAR_HEIGHT, 0, object : AnimationListener {
                    override fun onAnimationCompleted() {
                        isAnimating = false
                        isSearchVisible = false
                        searchView.visibility = View.GONE
                    }

                    override fun onAnimationCanceled() {
                        isAnimating = false
                    }
                })
            }
            lastPosition = currentPosition
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = ItemAdapter()
            addOnScrollListener(scrollListener)
        }
    }

    fun animate(view: View, fromHeight: Int, toHeight: Int, callback: AnimationListener?) {
        view.clearAnimation()
        view.visibility = View.VISIBLE
        val valueAnimator: ValueAnimator = ValueAnimator.ofInt(fromHeight, toHeight)

        valueAnimator.addUpdateListener { animation ->
            val layoutParam = view.layoutParams
            layoutParam.height = animation.animatedValue as Int
            view.layoutParams = layoutParam
        }

        valueAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                callback?.onAnimationCompleted()
            }

            override fun onAnimationCancel(p0: Animator?) {
                callback?.onAnimationCanceled()
            }

            override fun onAnimationStart(p0: Animator?) {
            }

        })

        valueAnimator.interpolator = DecelerateInterpolator()
        valueAnimator.duration = 200
        valueAnimator.start()
    }

    interface AnimationListener {
        fun onAnimationCompleted()
        fun onAnimationCanceled()
    }
}